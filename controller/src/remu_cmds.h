/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: remu   //  RASHPA emulator
 *
 * $URL: $
 * $Rev: $
 * $Date: $
 *
 *------------------------------------------------------------------------- */

#ifndef __APP_CMDS_H_INCLUDED__
#define __APP_CMDS_H_INCLUDED__

// Hsz Vsz bdepth Xoff Yoff noise fps
#define fmtDATASRC                                                             \
  "( {T#CONFIG I I I I I F } 	| \
							T#DROP  				| \
						   {T#ADD L } 				  \
						)"

#define fmtDATASET                                                             \
  "L (T#REMOVE  				| \
						   {T#CREATE I L } 			| \
						   {T#MEMCPY I L } 			| \
						   {T#RANDOM I } 		      \
						)"

#define fmtREMU                                                                \
  "( 						 	  \
							T#RESET  				| \
						   {T#START}				| \
						   {T#STARTLINK}			| \
							T#STOP   				  \
						)"

#define APPMENU_LIST                                                           \
  MENU_IT(bRASHPACONF, fmtNONE, fmtNONE, _B, "process incomming telegram")     \
  MENU_IT(qbTELEGRAM, "I", fmtNONE, _B, "return telegram")                     \
  MENU_IT(REMU, fmtREMU, fmtNONE, _B, "set detector acquisition mode")         \
  MENU_IT(qREMU, fmtNONE, fmtSTRING, _B, "query REMU status")                  \
  MENU_IT(bDATASET, "L I I I", fmtNONE, _B,                                    \
          "upload reference image or whole dataset")                           \
  MENU_IT(DATASET, fmtDATASET, fmtNONE, _B,                                    \
          "create/remove sequence of predefined images")                       \
  MENU_IT(qDATASET, fmtNONE, fmtSTRING, _B, "list existing dataset")           \
  MENU_IT(qbDATASET, "L I", fmtNONE, _B, "return image #imgseq #img")          \
  MENU_IT(DATASRC, fmtDATASRC, fmtNONE, _B,                                    \
          "config source size HxV offset XxY maxrate f, add/remove dataset")   \
  MENU_IT(qDATASRC, fmtNONE, fmtSTRING, _B,                                    \
          "query source configuration and active dataset")

#define APPLISTS DANCELIST(BITDEPTH)

#define BITDEPTH_4 0
#define BITDEPTH_8 1
#define BITDEPTH_16 2
#define BITDEPTH_32 3
#define BITDEPTH_64 4
#define BITDEPTH_LIST                                                          \
  { "4bits", "8bits", "16bits", "32bits", "64bits" }
#define BITDEPTH_TO_BITS(choice) (4 << (choice))

#endif // __APP_CMDS_H_INCLUDED__
