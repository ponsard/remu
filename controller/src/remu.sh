#!/bin/bash
cd remu/controller/src

source /segfs/deg/bin/degprofile.sh

make clean
rm /tmp/controller
make build 

gcc -o /tmp/controller -I ~/LIBDANCE/rem/trunk/src/ \
 remu_imgproc.o controller.o rashpa_rsocket.o  \
~/librashpa/src/rashpa_xml.o ~/librashpa/src/rashpa_init.o \
~/librashpa/src/rashpa_roce_ud.o ~/librashpa/src/rashpa_zmq.o \
~/librashpa/src/rashpa_main.o ~/librashpa/src/rashpa_udp.o \
-L /usr/lib/x86_64-linux-gnu/   \
-L ~/LIBDANCE/rem/trunk/src/ \
-L /users/ponsard/LIBDANCE/rem/trunk/libs/libepci/src/ \
-L /users/ponsard/LIBDANCE/rem/trunk/libs/libedma/src/ \
-L /users/ponsard/LIBDANCE/rem/trunk/libs/libebuf/src/ \
-L /users/ponsard/LIBDANCE/rem/trunk/libs/libuirq/src/ \
-L /users/ponsard/LIBDANCE/rem/trunk/libs/libuirq/src/ \
-L /users/ponsard/LIBDANCE/rem/trunk/libs/libuirq/src/u/ \
-L /users/ponsard/LIBDANCE/rem/trunk/libs/libefpak/src/ \
-L /users/ponsard/LIBDANCE/rem/trunk/libs/libespi/src/ \
-L /users/ponsard/LIBDANCE/rem/trunk/libs/libpmem/src/u \
-L /users/ponsard/LIBDANCE/rem/trunk/libs/libejtag/src/ \
-L /users/ponsard/LIBDANCE/rem/trunk/libs/libefspi/src/ \
-L ~/tcc-0.9.27/ -L ~/ \
-ldance -lepci -lpci -lefpak -lespi -lz -ledma -luirq -lebuf -lpmem -lejtag -lefspi \
-lm -ldl -lrt -lpthread -lxml2 -libverbs -lrdmacm -ltcc -lzmq -lnuma

################################################################################


LIBVMA=$VMA

echo 'REMU DAnCE Controller.....' 

if [ "$LIBVMA" = "1" ]; then
echo "using libVMA... Incompatibility with RoCE"
LD_PRELOAD=/usr/lib/libvma.so  \
LD_LIBRARY_PATH=~/tcc-0.9.27:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libepci/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libedma/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libebuf/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libuirq/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libuirq/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libuirq/src/u/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libefpak/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libespi/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libpmem/src/u/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libejtag/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libefspi/src/:\
../../../tcc-0.9.27/ \
/tmp/controller  -lAPP 
else
LD_LIBRARY_PATH=~/tcc-0.9.27:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libepci/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libedma/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libebuf/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libuirq/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libuirq/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libuirq/src/u/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libefpak/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libespi/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libpmem/src/u/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libejtag/src/:\
/users/ponsard/LIBDANCE/rem/trunk/libs/libefspi/src/:\
../../../tcc-0.9.27/ \
/tmp/controller  -lAPP
fi


