#!/bin/bash
RASHPA_DIR=/segfs/deg/working/ponsard/rashpa_poc
# RASHPA_DIR=/users/ponsard/rashpa_poc
# RASHPA_DIR=/tmp_14_days/ponsard/rashpa_poc

cd $RASHPA_DIR/remu/controller/src

source /segfs/deg/bin/degprofile.sh

make clean
rm /tmp/controller
make build 

DANCE_DIR=$RASHPA_DIR/remu/controller/lib/libdance/trunk/libs
R_OBJ=${RASHPA_DIR}/romulu/controller/src/scisoft14

gcc -o /tmp/controller -I ../ \
remu_imgproc.o controller.o rashpa_rsocket.o  \
${R_OBJ}/rashpa_xml.o $R_OBJ/rashpa_init.o \
$R_OBJ/rashpa_roce_uc.o $R_OBJ/rashpa_zmq.o \
$R_OBJ/rashpa_main.o $R_OBJ/rashpa_udp.o \
-L /usr/lib/x86_64-linux-gnu/   \
-L ../src/ -L ../../.. \
-L $DANCE_DIR/../src/ \
-L $DANCE_DIR/libepci/src/ \
-L $DANCE_DIR/libedma/src/ \
-L $DANCE_DIR/libebuf/src/ \
-L $DANCE_DIR/libuirq/src/ \
-L $DANCE_DIR/libuirq/src/ \
-L $DANCE_DIR/libuirq/src/u/ \
-L $DANCE_DIR/libefpak/src/ \
-L $DANCE_DIR/libespi/src/ \
-L $DANCE_DIR/libpmem/src/ \
-L $DANCE_DIR/libejtag/src/ \
-L $DANCE_DIR/libefspi/src/ \
-L $RASHPA_DIR/tcc-0.9.27/ -L $RASHPA_DIR/ \
-ldance -lepci -lpci -lefpak -lespi -lz -ledma -luirq -lebuf -lpmem -lejtag -lefspi \
-lm -ldl -lrt -lpthread -lxml2 -libverbs -lrdmacm -ltcc -lzmq -lnuma -lpci


################################################################################


LIBVMA=$VMA

echo 'REMU DAnCE Controller.....' 

if [ "$LIBVMA" = "1" ]; then
echo "using libVMA... Incompatibility with RoCE"
LD_PRELOAD=/usr/lib/libvma.so  \
LD_LIBRARY_PATH=$RASHPA_DIR/tcc-0.9.27:\
${DANCE_DIR}//libefspi/libedma/src/:\
${DANCE_DIR}//libebuf/src/:\
${DANCE_DIR}//libuirq/src/:\
${DANCE_DIR}//libuirq/src/:\
${DANCE_DIR}//libuirq/src/u/:\
${DANCE_DIR}//libefpak/src/:\
${DANCE_DIR}//libespi/src/:\
${DANCE_DIR}//libpmem/src/:\
${DANCE_DIR}//libejtag/src/:\
${DANCE_DIR}//libefspi/src/:\
${DANCE_DIR}//libepci/src/:\
../../../tcc-0.9.27/ \
/tmp/controller  -lAPP
else
LD_LIBRARY_PATH=$DANCE_DIR/tcc-0.9.27:\
${DANCE_DIR}//libepci/src/:\
${DANCE_DIR}//libedma/src/:\
${DANCE_DIR}//libebuf/src/:\
${DANCE_DIR}//libuirq/src/:\
${DANCE_DIR}//libuirq/src/:\
${DANCE_DIR}//libuirq/src/u/:\
${DANCE_DIR}//libefpak/src/:\
${DANCE_DIR}//libespi/src/:\
${DANCE_DIR}//libpmem/src/:\
${DANCE_DIR}//libejtag/src/:\
${DANCE_DIR}//libefspi/src/:\
../../../tcc-0.9.27/ \
/tmp/controller  -lAPP
fi


