import cv2
import numpy as np
from PIL import Image



#load and convert to Gray scale, float32
def loadImage(fname,s = (512,512)):
    im = Image.open(fname)
    im = im.convert('L')
    im = im.resize(s)
    return np.asarray(im, dtype=np.float32)


#output ready for REMU download
def createDatasetWithTag(img,w,h,nframes):
  
    dataset = [img]
    for i in range(1,nframes):
        im1 = np.copy(img)

        val=255 if i%3 == 0 else 128
        cv2.putText(im1,str(i), (0,  int(h/5)),          cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255),2)
        cv2.putText(im1,str(i), (int(w*1/5),int(h*2/5)), cv2.FONT_HERSHEY_SIMPLEX, 2, (255,255,255),2)
        cv2.putText(im1,str(i), (int(w*2/5),int(h*3/5)), cv2.FONT_HERSHEY_SIMPLEX, 16, (val,val,val),40,20)
        cv2.putText(im1,str(i), (int(w*3/5),int(h*4/5)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255),2)
        cv2.putText(im1,str(i), (int(w*4/5),int(h*4/5)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255),2)
      
        dataset = np.append(dataset,im1.reshape((w*h,)))
    return dataset


#imshow
def getImgFromDataset(dsname,w,h,b):
    n = int(len(dsname.tobytes())/b/w/h)
    return np.frombuffer(dsname,dtype=np.float32).reshape((n,h,w))


#remove Header data from Jungfrau dectector images
def remove_header_jfImage(d,width,height,nImg,hdr):
    data = d
    totalSizeInFloat16 = int(len(data.tobytes())/2)
    
    print('compute header data')
    hdrl=[]
    for i in range(0,totalSizeInFloat16,width*height+hdr):
        l=[]
        for k in range(i, i+hdr):
            l.extend([k])
        hdrl.extend(l)
       
    print('deleting header data')
    dataset = np.delete(data,hdrl)
    
    return dataset.astype(dtype=np.float32)
