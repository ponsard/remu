/*
 *
 * dataset processing should rather be done in remu python client application
 *
 *
 *
 *
 *
 *
 *
 */
#include "libdance.h"
#include "remu.h"

void create_image_seq(char *dst, int width, int height, int bdepth, char *src,
                      int noise) {
  int i, ii, kk, j;
  char *mask;

  // apply dataset MASK if exists, same size
  int apply_mask = 0;
  dataset_t *dset = find_dataset(g_remu, "MASK");
  if (dset != NOT_DATASET && dset != BAD_DATASET) {
    mask = mblock_ptr(&dset->data);
    if (dset->hsz == g_remu->conf.hsz && dset->vsz == g_remu->conf.vsz)
      apply_mask = 1;
  }

  for (i = 0; i < width; i++)
    for (j = 0; j < height; j++)
      if (i < g_remu->conf.hsz && j < g_remu->conf.vsz) {
        // TODO bdepth : only 24  8 bits, center image ?
        ii = i + j * width;
        kk = i + j * g_remu->conf.hsz;
        if (bdepth == 8 && g_remu->conf.bdepth == 8) {
          *(dst + kk + 0) = (src[ii + 0] + (random() % (noise + 1))) &
                            ((apply_mask) ? mask[kk] : 0xFF);
        }
        if (bdepth == 8 && g_remu->conf.bdepth == 24) {
          *(dst + 3 * kk + 0) = (src[ii] + (random() % (noise + 1))) &
                                ((apply_mask) ? mask[kk] : 0xFF);
          *(dst + 3 * kk + 1) = (src[ii] + (random() % (noise + 1))) &
                                ((apply_mask) ? mask[kk] : 0xFF);
          *(dst + 3 * kk + 2) = (src[ii] + (random() % (noise + 1))) &
                                ((apply_mask) ? mask[kk] : 0xFF);
        }
        if (bdepth == 24 && g_remu->conf.bdepth == 8) {
          *(dst + kk) =
              ((src[3 * ii + 0] + src[3 * ii + 1] + src[3 * ii + 2]) / 3 +
               (random() % (noise + 1))) &
              ((apply_mask) ? mask[kk] : 0xFF);
        }
        if (bdepth == 24 && g_remu->conf.bdepth == 24) {
          *(dst + 3 * kk + 0) = (src[3 * ii + 0] + (random() % (noise + 1))) &
                                ((apply_mask) ? mask[kk] : 0xFF);
          *(dst + 3 * kk + 1) = (src[3 * ii + 1] + (random() % (noise + 1))) &
                                ((apply_mask) ? mask[kk] : 0xFF);
          *(dst + 3 * kk + 2) = (src[3 * ii + 2] + (random() % (noise + 1))) &
                                ((apply_mask) ? mask[kk] : 0xFF);
        }
      }
}
