/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: remu   //  RASHPA emulator
 *
 * $URL: $
 * $Rev: $
 * $Date: $
 *------------------------------------------------------------------------- */

#ifndef __REMU_H_INCLUDED__
#define __REMU_H_INCLUDED__

#include "libdance.h"
#include "dancemain.h"
#include <stdint.h>

typedef unsigned long int size_t;
// REMU MAX
#define MAX_N_DATASETS 16
#define MAX_N_DATASRC 8
#define MAX_N_ROI 8
#define NUM_THREADS 32
#define MAX_NAME_LENGTH 31

// REMU enum
#define IDS_MANDATORY (1 << 0)
#define IDS_CANBENEW (1 << 1)
#define IDS_NEEDSIMAGE (1 << 8)

#define BAD_DATASET ((dataset_t *)(-1))
#define NOT_DATASET NULL

#define DS_UNUSED (0xaa)
#define DS_VARSZ (1 << 0)
#define DS_FIXSZ (1 << 1)
#define DS_IMAGE (1 << 2)
#define DS_ANYTYPE (DS_VARSZ | DS_FIXSZ | DS_IMAGE)

#define DEF_TYPE DS_UNUSED
#define DEF_IMSIZE 512
#define DEF_BDEPTH 16

// REMU data types
typedef struct {
  int type; // type of data set
  char name[MAX_NAME_LENGTH + 1];
  size_t size; // total data set size in bytes
  int hsz;     //  for image only
  int vsz;     // 	"	"
  int bdepth;  // 	"	"
  mblock_t data;
  size_t nframes; // 1 for ref or mask
} dataset_t;

#define DEFAULT_DATASET                                                        \
  {                                                                            \
    .type = DEF_TYPE, .name = "",                                              \
    .size = DEF_IMSIZE * DEF_IMSIZE * (DEF_BDEPTH / 8), .hsz = DEF_IMSIZE,     \
    .vsz = DEF_IMSIZE, .bdepth = DEF_BDEPTH, .data = 0, .nframes = 0           \
  }

// configuration data of the Detector Module
typedef struct {
  int id;       // id of the module in Detector
  uint32_t hsz; // horizontal image size in pixels
  uint32_t vsz; // vertical image size in pixels
  uint32_t bdepth;
  uint32_t hoff; // horizontal image offset in pixels
  uint32_t voff; // vertical image offset in pixels
  double maxrate;
} remu_conf_t;

#define DEFAULT_REMUCONF                                                       \
  {                                                                            \
    .hsz = DEF_IMSIZE, .vsz = DEF_IMSIZE, .bdepth = 0, .hoff = 0, .voff = 0,   \
    .maxrate = 0,                                                              \
  }

typedef struct remu_globals_s {
  remu_conf_t conf;                  // source main configuration
  dataset_t dataset[MAX_N_DATASETS]; // selection of available dataset
  dataset_t *src[MAX_N_DATASRC];     // active dataset for this src
  int srcIndex;                      // number of active datasource 0...srcIndex
  int nThread;                       // FIXME same as srcIndex ?
  //~ state_t 			state;						// source
  //status
} remu_globals_t;

extern remu_globals_t *g_remu;
extern dataset_t *find_dataset(remu_globals_t *remu, const char *name);

#endif //  __REMU_H_INCLUDED__
