import sys
import numpy as np

# sys.path.append('/tmp_14_days/ponsard/rashpa_poc/')
sys.path.append('/segfs/deg/working/ponsard/rashpa_poc/')
from libdeep.trunk.python.libdeep import DeepDevice, DeepArray



#########
# 4 GB limit in libdeep due to malloc rlimits
#
#
def config_remu():
    remu = DeepDevice('localhost')
    
    remu.command('REMU STOP')
    
    remu.command('DATASRC CONFIG '+str(100)+' '+str(200)+' '+str(8*3)+'  0 0 0.0')
    
#     for m in ['LZ-512x1024-1000']:
    for m in ['test-data']:
#    for m in ['ID-512x1024']:
#     for m in ['TP-JF-512x1024','TP-1024x512','TP-2048x2048',                #teapot
#                'LZ-JF-1000x1024x512','LZ-1000x1024x512','LZ-100x2048x2048',  #Lyzozym
#                ]:
        #teapot demo ,'TS-10000x1024x512' ,'TS-1000x1024x512'
        choice=1
        if m=='test-data':
            if choice==1:
                width,height,nImg,bdepth = 1024,2*512,1000,2
                ds = np.fromfile('/tmp/lz_500k_1000.data',dtype=np.uint16)
            if choice==2:
                ds = np.fromfile('/tmp/lz_500k_100.data',dtype=np.uint16)
                width,height,nImg,bdepth = 1024,2*512,100,2
            if choice==3:
                width,height,nImg,bdepth = 2048,2*2048,100,2
                ds = np.fromfile('/tmp/lz_4M_100.data',dtype=np.uint16)
            # width,height,nImg,bdepth = 1024,2*512,100,2
            # ds = np.fromfile('/tmp/pyfai_p_g.data',dtype=np.uint16)
            # ds = np.fromfile('/tmp/tp100_500k_gp.data',dtype=np.uint16) 
        if m=='LZ-512x1024-1000':
            width,height,nImg,bdepth = 1024,2*512,100,2
            ds = np.fromfile('/tmp/lz_500k_1000.data',dtype=np.uint16)
            
        if m=='ID-512x1024':
            width,height,nImg,bdepth = 1024,2*512,100,2
            ds = np.fromfile('/tmp/id.data',dtype=np.uint16)
            
        if m=='TP-2048x2048':
            width,height,nImg,bdepth = 2048,2*2048,100,2
            ds = np.fromfile('/tmp/ds_teapot4k_100_gain_ped.data',dtype=np.uint16)
        if m=='TP-1024x512':
            width,height,nImg,bdepth = 1024,512*2,100,2
            ds = np.fromfile('/tmp/tp100_gp.data',dtype=np.uint16)
        if m=='TP-JF-512x1024':
            width,height,nImg,bdepth = 8246,128*2,1000,1
            ds = np.fromfile('/tmp/teapot1000asJF.data',dtype=np.uint8)

        #Lyzozym data
        if m=='LZ-JF-1000x1024x512': 
            width,height,nImg,bdepth = 8246,128*2,1000,1
            ds = np.fromfile('/tmp/m10_p_g_h.data',dtype=np.uint8)
        if m=='LZ-1000x1024x512':
            width,height,nImg,bdepth = 1024,512*2,1000,2
            ds = np.fromfile('/tmp/m00_p_g.data',dtype=np.uint16)
        if m=='LZ-100x2048x2048':  
            width,height,nImg,bdepth = 2048,2048*2,100,2
            ds = np.fromfile('/tmp/ds_lz_4k_100_gain_ped.data',dtype=np.uint16)
        
        if m=='TS-1000x1024x512':  
            width,height,nImg,bdepth = 1024,512*2,1000,2
            ds = np.fromfile('/tmp/test.data',dtype=np.uint16)
        a = DeepArray(ds.tobytes())
        
        if m=='TS-10000x1024x512':  
            width,height,nImg,bdepth = 1024,512*2,1000,2
            ds = np.fromfile('/tmp/test10k.data',dtype=np.uint16)
        a = DeepArray(ds.tobytes())
        
        cmd='REMU STOP'
        print(cmd)
        remu.command(cmd)
        cmd='*DATASET '+m+' '+str(width)+' '+str(height)+' '+str(nImg)
        print(cmd)
        remu.command(cmd,a)
        cmd='DATASRC ADD '+m+' '
        print(cmd)
        remu.command(cmd)
        
        
config_remu()



def config_multiremu():
    N,h,w=100,512,1024
    #images
    for c in [0,1]:
        for r in [0,1,2,3]:
            f='/tmp/m'+str(c)+str(r)+'.data'
            ds = np.fromfile(f,dtype=np.uint16)

            remu = DeepDevice('localhost:500'+str(r+4*c))
            remu.command('REMU STOP')
            #not really used
            remu.command('DATASRC CONFIG '+str(N)+' '+str(200)+' '+str(8*3)+'  0 0 0.0')
            width,height,nImg,bdepth = w,h,N,2

            a = DeepArray(ds.tobytes())

            remu.command('REMU STOP')
            remu.command('*DATASET M'+str(r+4*c)+' '+str(w)+' '+str(h)+' '+str(N),a)
            remu.command('DATASRC ADD M'+str(r+4*c)+' ')
            remu.command('?DATASET')
            remu.command('?DATASRC')
            
    #pedestal        
    for c in [0,1]:
        for r in [0,1,2,3]:
            f='/tmp/p'+str(c)+str(r)+'.data'
            ds = np.fromfile(f,dtype=np.uint16)

            #print('----'+str(8+r+4*c))
            remu = DeepDevice('localhost:%d'%(5008+r+4*c))
            
            remu.command('REMU STOP')
            #not really used
            remu.command('DATASRC CONFIG '+str(N)+' '+str(200)+' '+str(8*3)+'  0 0 0.0')
            width,height,nImg,bdepth = w,h,N,2

            a = DeepArray(ds.tobytes())

            remu.command('REMU STOP')
            remu.command('*DATASET P'+str(r+4*c)+' '+str(w)+' '+str(h)+' '+str(N),a)
            remu.command('DATASRC ADD P'+str(r+4*c)+' ')
            remu.command('?DATASET')
            remu.command('?DATASRC')        
            
            
#config_multiremu()       
