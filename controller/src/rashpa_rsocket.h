
#ifndef __RASHPA_RSOCKET_H_INCLUDED__
#define __RASHPA_RSOCKET_H_INCLUDED__

#include <arpa/inet.h>

union rsocket_address {
  struct sockaddr sa;
  struct sockaddr_in sin;
  struct sockaddr_in6 sin6;
  struct sockaddr_storage storage;
};

extern void *rsocket_datachannel(void *arg);

#endif
