/*
 *
 * first implementation using rsocket/SoftIWarp
 *
 * pending work : ibverbs/RoCEv2
 * or any other combination
 *
 */

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "librashpa.h"
#include "rashpa_rsocket.h"
#include <rdma/rsocket.h>

extern rashpa_ctx_t g_rashpa;

static char sPort[REMOTE_HOST_PORT_MAX];
static char dst_addr[HOST_NAME_MAX];

void *rsocket_datachannel(void *arg) {
  rashpa_ctx_t *link = &g_rashpa;

  void *src_addr = link->source_addr;
  strcpy(dst_addr, link->hostname);
  int port = link->port;

  printf("rsocket_datachannel %s %d\n", dst_addr, port);

  int ret, rs;
  struct addrinfo *res;

  snprintf(sPort, REMOTE_HOST_PORT_MAX, "%d", port);
  ret = getaddrinfo(dst_addr, sPort, NULL, &res);
  if (ret) {
    printf("getaddrinfo failed: %s\n", gai_strerror(ret));
    return NULL;
  }

  rs = rsocket(res->ai_family, res->ai_socktype, res->ai_protocol);
  if (rs < 0) {
    printf("rsocket failed\n");
    goto free;
  }

  ret = rconnect(rs, res->ai_addr, res->ai_addrlen);
  if (ret) {
    printf("rconnect failed\n");
    goto rc;
  }

  if (rs < 0)
    return NULL;

#define MYCLOCK CLOCK_MONOTONIC
  struct timespec beg, endTime;
  clock_gettime(MYCLOCK, &beg);

  uint64_t len = rsend(rs, src_addr, link->dsSize, 0);

  clock_gettime(MYCLOCK, &endTime);
  double dt_ms = (endTime.tv_nsec - beg.tv_nsec) / 1000000.0 +
                 (endTime.tv_sec - beg.tv_sec) * 1000.0;
  double Bps = len / dt_ms * 1e3;
  double roMBps = Bps / 1024.0 / 1024.0;

  printf("REMU sent %ld bytes of %ld using RSOCKET, at %f MB/s\n", len,
         link->dsSize, roMBps);
  rshutdown(rs, SHUT_RDWR);
rc:
  rclose(rs);
free:
  freeaddrinfo(res);
  return NULL;
}
