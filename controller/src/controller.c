/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: remu   //  RASHPA emulator
 *
 *
 * - manage dataset (download/upload)
 * - create datasource (using 1 dataset)
 * - process RASHPA configuration telegram (LINK, ROI)
 * - start (multiple) transfer
 *
 *
 *
 * $URL: $
 * $Rev: $
 * $Date: $
 *------------------------------------------------------------------------- */

#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>



#include "dancemain.h"
#include "remu.h"
#include "remu_imgproc.h"

//~ #include "../../testclient/gdrcopy/gdrapi.h"
#include "librashpa.h"
#include "rashpa_init.h"
#include "rashpa_main.h"
#include "rashpa_roce_uc.h"
#include "rashpa_rsocket.h"
#include "rashpa_zmq.h"

DANCE_APPNAME("REMU");
DANCE_VERSION(0, 1);




// FIXME is it really needed ?
pthread_t threads[NUM_THREADS];
remu_globals_t *g_remu;

extern rashpa_ctx_t g_rashpa;
extern roce_ctx_t g_roce;

dataset_t *find_dataset(remu_globals_t *remu, const char *name) {
  for (int i = 0; i < MAX_N_DATASETS; i++) {
    dataset_t *dataset = &remu->dataset[i];
    if ((dataset->type != DS_UNUSED) && strcmp(dataset->name, name) == 0) {
      return dataset;
    }
  }

  return NULL; /// if not existing dataset
}

void clear_dataset(int releaseMem) {
  dataset_t *dataset;
  for (int i = 0; i < MAX_N_DATASETS; i++) {
    dataset = &g_remu->dataset[i];

    if (releaseMem && dataset->type != DS_UNUSED)
      mblock_release(&dataset->data, DESTROY_DEFAULT);

    dataset->type = DS_UNUSED;
  }
}

void release_dataset(const char *datasetName) {
  dataset_t *dset = find_dataset(g_remu, datasetName);
  if (dset == NOT_DATASET || dset == BAD_DATASET) {
    LOG_APP("DATASET %s NOT EXISTING\n", datasetName);
    return;
  }
  mblock_release(&dset->data, DESTROY_DEFAULT);
  dset->type = DS_UNUSED;
}

static dataset_t *find_dataset_slot(remu_globals_t *remu) {
  dataset_t *dataset;
  int i;

  for (i = 0; i < MAX_N_DATASETS; i++) {
    dataset = &remu->dataset[i];
    if (dataset->type == DS_UNUSED) {
      return dataset;
    }
  }
  return NULL;
}

dataset_t *parse_dataset(const char *name, int flags) {
  if (name == NULL)
    name = I_LABEL();
  if (name == DEFAULT_LABEL) {
    if (flags & IDS_MANDATORY) {
      errorf("missing dataset name\n");
      return BAD_DATASET;
    } else {
      return NOT_DATASET;
    }
  } else {
    dataset_t *dset = find_dataset(g_remu, name);
    if (dset == NULL) {
      if (flags & IDS_CANBENEW) {
        dset = find_dataset_slot(g_remu);
        if (dset == NULL) {
          errorf("maximum number of data sets (%d) reached\n", MAX_N_DATASETS);
          return BAD_DATASET;
        } else if (strlen(name) > MAX_NAME_LENGTH) {
          errorf("data set name %s is too long\n", name);
          return BAD_DATASET;
        } else {
          strcpy(dset->name, name);
        }
      } else {
        errorf("%s is not a existing dataset name\n", name);
        return BAD_DATASET;
      }
    } else if ((flags & IDS_NEEDSIMAGE) && dset->type != DS_IMAGE) {
      errorf("%s is not an image data set\n", name);
      return BAD_DATASET;
    }

    return dset;
  }
}

static void answer_dataset(dataset_t *dataset) {
  switch (dataset->type) {
  case DS_VARSZ:
    answerf("VARSZ");
    break;

  case DS_FIXSZ:
    answerf("FIXSZ %d", dataset->size);
    break;

  case DS_IMAGE:
    answerf("\t\tIMAGE %dx%d \t\t%dbits \t\t size %lu \t\t%d frames",
            dataset->hsz, dataset->vsz, dataset->bdepth, dataset->size,
            dataset->nframes);
    break;

  default:
    answerf("UNDEFINED DATASET TYPE");
    break;
  }
}

static void answer_all_datasets(remu_globals_t *remu, int withcmd) {
  dataset_t *dataset = remu->dataset;
  // TODO remu_conf_t* conf = remu->conf;

  const char *prefix = withcmd ? "DATASET " : "";
  int nsets = 0;
  int maxlength = 0;
  int i;

  for (i = 0; i < MAX_N_DATASETS; i++) {
    int length = strlen(dataset[i].name);
    if (length > maxlength)
      maxlength = length;
  }
  for (i = 0; i < MAX_N_DATASETS; i++) {
    dataset_t *dset = &dataset[i];
    if (dset->type != DS_UNUSED) {
      nsets++;
      answerf("%s \t\t%s \t\t", prefix, dset->name);
      answer_dataset(dset);
      answerf("\n");
    }
  }
  if (nsets == 0)
    answerf("\n");
}

void exec_DATASRC(void) {
  rashpa_ctx_t *link = &g_rashpa;

  if (I_NAMED_TAG("CONFIG") &&
      (link->remu_state == REMU_IDLE || link->remu_state == REMU_LINK_READY)) {
    // LOG_APP("DATASOURCE CONFIGURATION\n");

    // those conf parameters are NOT USED YET but REMU state must be set to
    // CONFIGURED to continue...
    g_remu->conf.hsz = I_INTEGER();
    g_remu->conf.vsz = I_INTEGER();
    g_remu->conf.bdepth = I_INTEGER();
    g_remu->conf.hoff = I_INTEGER();
    g_remu->conf.voff = I_INTEGER();
    g_remu->conf.maxrate = I_FLOAT();
    link->remu_state = REMU_SOURCE_CONFIGURED;
  } else if (I_NAMED_TAG("ADD") &&
             (link->remu_state == REMU_SOURCE_CONFIGURED ||
              link->remu_state == REMU_LINK_READY) &&
             g_remu->srcIndex < MAX_N_DATASRC) {
    const char *datasetName = I_LABEL();
    dataset_t *dset = parse_dataset(datasetName, IDS_MANDATORY);
    if (dset == BAD_DATASET || dset == NOT_DATASET) {
      LOG_APP("NOT EXISTING DATASET\n");
      return;
    }
    g_remu->src[g_remu->srcIndex] = dset;
    LOG_APP("DATASET %s ADDED TO SOURCE %d\n",
            g_remu->src[g_remu->srcIndex]->name, g_remu->srcIndex);
    g_remu->srcIndex++;
  } else if (I_NAMED_TAG("DROP") && link->remu_state <= REMU_LINK_READY &&
             g_remu->srcIndex > 0) {
    g_remu->srcIndex--;
  } else {
    LOG_APP("CONFIGURATION NOT POSSIBLE IN THIS STATE\n");
    return;
  }
}

void exec_qDATASRC(void) {
  rashpa_ctx_t *link = &g_rashpa;

  if (link->remu_state < REMU_SOURCE_CONFIGURED)
    answerf("DATA SOURCE NOT YET CONFIGURED\n");
  else {
    answerf("DATASOURCE size %ldx%d bdepth %d offset %dx%d fps %f\n",
            g_remu->conf.hsz, g_remu->conf.vsz, g_remu->conf.bdepth,
            g_remu->conf.hoff, g_remu->conf.voff, g_remu->conf.maxrate);
    int i;
    for (i = 0; i < g_remu->srcIndex; i++) {
      answerf("SELECTED DATASET %d %s\n", i, g_remu->src[i]->name);
    }
  }
}

extern int process_rashpa_telegram_for_remu(char *rashpa_manager_request,
                                            rashpa_ctx_t *link,
                                            roce_ctx_t *roce);

// incoming telegram
void exec_bRASHPACONF(void) {
  rashpa_ctx_t *link = &g_rashpa;
  roce_ctx_t *roce = &g_roce;
  binary_t *binfo = get_binary_com_info();
  uint64_t size = binfo->size * binfo->unit;
  mblock_t buffer;

  if (link->remu_state < REMU_SOURCE_CONFIGURED) {
    LOG_APP("REMU SOURCE NOT CONFIGURED\n");
    return;
  }

  if (link->remu_state == REMU_RUNNING) {
    LOG_APP("REMU STOP required\n");
    return;
  }

  // +1 required for printf
  if (mblock_create(&buffer, NULL, size + 1, MEM_DEFAULT | MEM_PERSISTENT) ==
      NULL) {
    LOG_APP("Cannot allocate internal memory (%ld bytes)\n", size);
    return;
  }

  if (get_binary_data(mblock_ptr(&buffer), binfo->unit, binfo->size) < 0) {
    LOG_APP("Error reading image data\n");
    mblock_release(&buffer, DESTROY_DEFAULT);
    return;
  }

  *(char *)(mblock_ptr(&buffer) + size) = 0;

  // FIXME one rashpa_id per DATACHANNEL
  //	link->rashpa_id = 0;

  process_rashpa_telegram_for_remu(mblock_ptr(&buffer), link, roce);

  int nSrc = link->data_source;
  if (nSrc < 0 || nSrc >= g_remu->srcIndex) {
    LOG_APP("Error bad src number\n");
    return;
  }

  dataset_t *ds = g_remu->src[nSrc];
  LOG_APP("RASHPA LINK %s WITH SRC %d addr %p\n",
          RASHPA_LINK_NAME[link->link_type], nSrc, mblock_ptr(&ds->data));

  link->source_addr = mblock_ptr(&ds->data);
  link->dsSize = ds->size; // include bdepth
  link->bdepth = ds->bdepth / 8;
  // link->nImages		= ds->nframes; //let user choose in telegram
  link->hsz = ds->hsz;
  link->vsz = ds->vsz;
  // LOG_APP("in controller link->bdepth %d hz %d vz
  // %d\n",link->bdepth,link->hsz,link->vsz);

  link->remu_state = REMU_LINK_READY;
  mblock_release(&buffer, DESTROY_DEFAULT);
}

int roi_ids[64];
void remu_start() {
  rashpa_ctx_t *link = &g_rashpa;

  int roi_number = link->roi_number;

  link->remu_state = REMU_RUNNING;

  switch (link->link_type) {
  case RASHPA_LINK_RSOCKET:
    if (pthread_create(&threads[g_remu->nThread], NULL, rsocket_datachannel,
                       (void *)&link)) {
      LOG_APP("ERROR LAUNCHING rsocket_datachannel THREAD\n");
    };
    break;

  case RASHPA_LINK_ROCEV2_UC:

    for (int i = 0; i < roi_number; i++)
      if (rashpa_rocev2_uc_detector_init(i) != RASHPA_LINK_OK)
        return;

    if (pthread_create(&threads[g_remu->nThread], NULL,
                       rashpa_rocev2_uc_detector_mainloop_single_thread,
                       (void *)&roi_ids[0])) {
      LOG_APP("ERROR LAUNCHING rashpa_rocev2_uc_detector_mainloop_single_thread\n");
    };
    break;

  case RASHPA_LINK_ROCEV2_RAW:
  case RASHPA_LINK_UDP_JF:
  case RASHPA_LINK_UDP:
  case RASHPA_LINK_ZMQ:

    if (rashpa_init(RASHPA_DETECTOR) != RASHPA_LINK_OK)
      return;

    if (pthread_create(&threads[g_remu->nThread], NULL,
                       rashpa_detector_mainloop, (void *)&roi_number)) {
      LOG_APP("ERROR LAUNCHING rashpa_detector_mainloop THREAD\n");
    };

    break;

  case RASHPA_LINK_MEMCOPY:
  case RASHPA_LINK_GDRCOPY:
  default:
    LOG_APP("LINK NOT IMPLEMENTED\n");
    return;
  }
}

// outgoing telegram skeleton
void exec_qbTELEGRAM(void) {
  int n = I_INTEGER(); // for edu only

  mblock_t buffer;
  uint64_t size = 1024;

  LOG_APP("qbTELEGRAM UPLOADING  %d\n", n);
  if (mblock_create(&buffer, NULL, size, MEM_DEFAULT | MEM_PERSISTENT) ==
      NULL) {
    errorf("Cannot allocate internal memory (%zd bytes)\n", size);
    return;
  }
  sprintf(mblock_ptr(&buffer), "<hello_esrf data = %d />", n);
  send_binary_buffer(&buffer, 1, size, BIN_FLG_NOCHKSUM);
  mblock_release(&buffer, DESTROY_DEFAULT);
}

void exec_qREMU(void) {
  rashpa_ctx_t *link = &g_rashpa;
  answerf("REMU STATE %s\n", REMU_STATE[link->remu_state]);
}

//  REMU {RESET | START | STOP | PAUSE}
void remu_reset() {
  rashpa_ctx_t *link = &g_rashpa;

  g_remu->srcIndex = 0;
  clear_dataset(1);
  int i;
  for (i = 0; i < g_remu->nThread; i++) {
    LOG_APP("THAT SHOULD NOT APPEAR remaining threads[i] = %d\n", threads[i]);
  }
  g_remu->nThread = 0;
  link->remu_state = REMU_IDLE;
}

void remu_stop() {
  rashpa_ctx_t *link = &g_rashpa;

  LOG_APP("remu_stop\n");

  // stop thread before releasing ressources (take some time)
  link->remu_state = REMU_SOURCE_CONFIGURED;
  // sleep(1);

  if (link->link_type == RASHPA_LINK_ROCEV2_UC)
    for (int i = link->roi_number - 1; i >= 0; i--)
      release_ib_context(RELEASE_RESET, i);
  else
    rashpa_fini(RASHPA_DETECTOR);
}

// remu state machine
void exec_REMU(void) {
  rashpa_ctx_t *link = &g_rashpa;

  // LOG_APP("REMU %s\n",REMU_STATE[link->remu_state]);

  switch (link->remu_state) {

  case REMU_IDLE:

    if (I_NAMED_TAG("RESET"))
      remu_reset();
    break;

  case REMU_SOURCE_CONFIGURED:

    if (I_NAMED_TAG("RESET"))
      remu_reset();
    break;

  case REMU_LINK_READY:

    if (I_NAMED_TAG("START"))
      remu_start();
    if (I_NAMED_TAG("RESET"))
      remu_reset();
    break;

  case REMU_RUNNING:

    if (I_NAMED_TAG("STOP"))
      remu_stop();
    if (I_NAMED_TAG("RESET"))
      remu_reset();
    break;
  }
}

// download dataset from remu
void exec_qbDATASET(void) {
  const char *datasetName = I_LABEL();
  int nImg = I_INTEGER();
  // LOG_APP("qbDISPLAY %s %d\n",datasetName,nImg);

  dataset_t *dset = parse_dataset(datasetName, IDS_MANDATORY);
  if (dset == BAD_DATASET || dset == NOT_DATASET) {
    LOG_APP("NOT EXISTING DATASET\n");
    return;
  }

  if ((nImg >= dset->nframes) || (nImg < 0)) {
    LOG_APP("IMG #%d NOT EXISTING IN %s\n", nImg, dset->name);
    return;
  }

  mblock_t *buffer = &dset->data;
  uint64_t size = dset->hsz * dset->vsz * dset->bdepth / 8;

  // FIXME Hugly hack because no offset in send_binary_buffer
  buffer->mbptr += nImg * size;
  send_binary_buffer(buffer, 1, size, BIN_FLG_NOCHKSUM);
  buffer->mbptr -= nImg * size;
}

// create dataset from imref
// DATASET <name> <nframes> <refImg> [NOISE <val> ] [FINCR <value>] [LINCR
// <val>] [PINCR <val>] [TAG]
void exec_DATASET(void) {
  rashpa_ctx_t *link = &g_rashpa;
  const char *datasetName = I_LABEL();

  if (I_NAMED_TAG("REMOVE")) {
    LOG_APP("REMOVE DATASET %s\n", datasetName);
    release_dataset(datasetName);
    return;
  }

  if (link->remu_state < REMU_SOURCE_CONFIGURED) {
    LOG_APP("CAN'T CREATE WHILE NOT IN CONFIGURED MODE\n");
    return;
  }

  int copymode = 0;
  int nFrames;
  const char *refName;
  if (I_NAMED_TAG("MEMCPY")) {
    nFrames = I_INTEGER();
    refName = I_LABEL();
    copymode = 1;
    LOG_APP("MEMCPY\n");
  } else if (I_NAMED_TAG("CREATE")) {
    nFrames = I_INTEGER();
    refName = I_LABEL();
    copymode = 2;
  } else if (I_NAMED_TAG("RANDOM")) {
    nFrames = I_INTEGER();
    refName = NULL;
    copymode = 3;
    goto create_ds;
  }

  LOG_APP("LOOKING FOR DATASET %s from REF %s frames %d\n", datasetName,
          refName, nFrames);

  dataset_t *ref = find_dataset(g_remu, refName);
  if (ref == NOT_DATASET || ref == BAD_DATASET) {
    LOG_APP("REFIMAGE %s NOT EXISTING\n", refName);
    return;
  }

  dataset_t *dset;

  dset = find_dataset(g_remu, datasetName);

  if (dset != NULL) {
    LOG_APP("ALREADY EXISTING DATASET NAME %s\n", datasetName);
    return;
  }

create_ds:
  LOG_APP("CREATING NEW DATASET %s frames %d\n", datasetName, nFrames);
  dset = parse_dataset(datasetName, IDS_MANDATORY | IDS_CANBENEW);
  LOG_APP("after parse_dataset\n");

  dset->type = DS_IMAGE;
  dset->hsz = g_remu->conf.hsz; // ref->hsz;
  dset->vsz = g_remu->conf.vsz; // ref->vsz;
  dset->bdepth = g_remu->conf.bdepth;
  dset->size = (uint64_t)dset->hsz * dset->vsz * dset->bdepth / 8 * nFrames;
  dset->nframes = 0;

  mblock_t *dstData = &dset->data, *refData = &ref->data;
  if (mblock_create(dstData, NULL, (uint64_t)dset->size,
                    MEM_DEFAULT | MEM_PERSISTENT) == NULL) {
    LOG_APP("Cannot allocate internal memory %lu\n ", dset->size);
    dset->type = DS_UNUSED;
    return;
  }

  int *ptr = (int *)mblock_ptr(dstData);

  while (dset->nframes < nFrames) {
    if (copymode == 2)
      create_image_seq(mblock_ptr(dstData) +
                           dset->size / nFrames * dset->nframes,
                       ref->vsz, ref->hsz, ref->bdepth, mblock_ptr(refData), 0);
    else if (copymode == 1)
      memcpy(mblock_ptr(dstData) + dset->size / nFrames * dset->nframes,
             mblock_ptr(refData), ref->vsz * ref->hsz * ref->bdepth / 8);
    else {
      LOG_APP("COPYING RANDOM DATA into %s\n", datasetName);
      for (size_t i = 0; i < dset->hsz * dset->vsz; i++)
        *(ptr + dset->nframes * dset->hsz * dset->vsz + i) = random();
    }

    // tag image with sequence id & frame number
    // *(int *)(mblock_ptr(dstData) + dset->size * dset->nframes / nFrames) =
    //     0xaaaaaaaa; // 0xbeef0000 |dset->nframes;
    dset->nframes++;
  }
}
// list dataset
void exec_qDATASET(void) { answer_all_datasets(g_remu, 0); }

// upload remote dataset into remu
void exec_bDATASET(void) {
  const char *datasetName = I_LABEL();
  int hsz = I_INTEGER();
  int vsz = I_INTEGER();
  int frames = I_INTEGER();

  binary_t *binfo = get_binary_com_info();
  dataset_t *dset = parse_dataset(datasetName, IDS_MANDATORY | IDS_CANBENEW);

  if (dset == NOT_DATASET) {
    LOG_APP("DATASET %s NOT EXISTING\n", datasetName);
    return;
  }

  mblock_t *buffer = &dset->data;
  size_t size = binfo->size * binfo->unit;
  if (size > (ssize_t) (0xFFFFFFFF)) {       
    LOG_ERROR("malloc can't allocate more than 4GB");
    printf("malloc can't allocate more than 4GB, but %lx\n",size);
    return;
  }
  if (mblock_create(buffer, NULL, size, MEM_DEFAULT | MEM_PERSISTENT) == NULL) {
    errorf("Cannot allocate internal memory (%zd bytes)\n", size);
    return;
  }

  // struct rlimit rl;
  // getrlimit(RLIMIT_AS,&rl);
  // printf("rlim_cur %u, rlim_max %u\n",rl.rlim_cur,rl.rlim_max);
  // getrlimit(RLIMIT_DATA,&rl);
  // printf("rlim_cur %u, rlim_max %u\n",rl.rlim_cur,rl.rlim_max);

  //limit to 4GB ???
  if (get_binary_data(mblock_ptr(buffer), binfo->unit, (__ssize_t)binfo->size) < 0) {
    errorf("Error reading image data\n");
    return;
  }

    // int f = open(datasetName, O_RDONLY, S_IRWXU);
    // if (f == -1) {
    //   printf(A_C_RED "error loading %s\n" A_C_RESET, link->gain_file);
    //   return NULL;
    // }
    // res = 0;

    // do {
    //   res += read(f, (void *)(((uint64_t)gainMap) + res), lenGainMap - res);
    //   debug_printf("gainmap res %d / %d\n", res, lenGainMap);
    // } while (res < lenGainMap);
    // debug_printf("first gain %f %f %f\n", *(gainMap + out_imgSize / 4),
    //        *(gainMap + 2 * out_imgSize / 4), *(gainMap + 3 * out_imgSize / 4));
    // close(f);

  strcpy(dset->name, datasetName);
  dset->nframes = frames;
  dset->size = size;
  dset->hsz = hsz;
  dset->vsz = vsz;
  dset->bdepth = 8 * size / (hsz * vsz * frames);
  dset->type = DS_IMAGE;
}

////////////////
////  REMU INIT
////////////////

static int remu_init(remu_globals_t *remu) {
  // initialise global data in *remu
  // and any other needed instrument initialisation
  rashpa_ctx_t *link = &g_rashpa;
  g_remu = remu;
  g_remu->srcIndex = 0;
  g_remu->nThread = 0;
  for (int i = 0; i < MAX_N_DATASETS; i++) {
    dataset_t *dataset = &remu->dataset[i];
    dataset->type = DS_UNUSED;
  }
  // remu->dataset = appconf_datasets;
  // remu->conf    = &appconf_remucfg;
  link->remu_state = REMU_IDLE;
  link->roi_number = 0;
  clear_dataset(0);

  return DANCE_OK;
}

/************************************/
/* instrument configuration routine */
/************************************/

// executed at the end of ?DCONFIG
void partial_exec_qDCONFIG(void) { answer_all_datasets(g_remu, 1); }

/***************************/
/* controller main routine */
/***************************/

int init_application(void) {
  static remu_globals_t remu_globals;
  printf("Initialisation of socket comm.version %s %s\n",__DATE__,__TIME__);

  if (remu_init(&remu_globals) != DANCE_OK) {
    LOG_ERROR("Initialisation of REMU failed.\n");
    return -1;
  }

  return 0;
}
