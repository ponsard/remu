# LIBDANCE_ROOT must be defined
ifeq ($(LIBDANCE_ROOT),)
   $(error Missing variable LIBDANCE_ROOT. Variable is not initialised)
endif

DANCE_DEFS_DIR ?= $(CURDIR)
export DANCE_DEFS_DIR

include ${LIBDANCE_ROOT}/build/dancetools.mk

DANCE_SDK_ROOT ?= /segfs/linux/dance_sdk
include ${DANCE_SDK_ROOT}/build/top.mk

# application files
DANCE_DEFS_H := dance_defs.h
CONTROLLER ?= controller

C_FILES += $(CONTROLLER).c
H_FILES += $(DANCE_DEFS_H)
O_FILES := $(C_FILES:.c=.o)
OTHER_O_FILES := $(filter-out $(CONTROLLER).o, $(O_FILES))

# dependency directories
LIBDANCE_DIR := $(LIBDANCE_ROOT)/src
LIBEPCI_DIR  := $(LIBDANCE_ROOT)/libs/libepci/src
LIBEFPAK_DIR := $(LIBDANCE_ROOT)/libs/libefpak/src
LIBUIRQ_DIR  := $(LIBDANCE_ROOT)/libs/libuirq/src/u
LIBEBUF_DIR  := $(LIBDANCE_ROOT)/libs/libebuf/src
LIBEDMA_DIR  := $(LIBDANCE_ROOT)/libs/libedma/src
LIBESPI_DIR  := $(LIBDANCE_ROOT)/libs/libespi/src
LIBEFSPI_DIR := $(LIBDANCE_ROOT)/libs/libefspi/src
LIBEJTAG_DIR := $(LIBDANCE_ROOT)/libs/libejtag/src
LIBPMEM_DIR  := $(LIBDANCE_ROOT)/libs/libpmem/src/u


# tools
CC := $(DANCE_SDK_CROSS_COMPILE)gcc
CFLAGS += $(C_FLAGS) -Wundef
CFLAGS += $(DANCE_SDK_CFLAGS)
CFLAGS += -I$(DANCE_SDK_DEPS_DIR)/include
CFLAGS += -I$(LIBDANCE_DIR)
CFLAGS += -I$(LIBEPCI_DIR)
CFLAGS += -I$(LIBEFPAK_DIR)
CFLAGS += -I$(LIBUIRQ_DIR)
CFLAGS += -I$(LIBEBUF_DIR)
CFLAGS += -I$(LIBEDMA_DIR)
CFLAGS += -I$(LIBESPI_DIR)
CFLAGS += -I$(LIBEFSPI_DIR)
CFLAGS += -I$(LIBPMEM_DIR)
CFLAGS += -I$(LIBEJTAG_DIR)

LFLAGS += $(L_FLAGS)
LFLAGS += $(DANCE_SDK_LFLAGS)
LFLAGS += -L$(DANCE_SDK_DEPS_DIR)/lib
LFLAGS += -L$(LIBDANCE_DIR)
LFLAGS += -L$(LIBEPCI_DIR)
LFLAGS += -L$(LIBEFPAK_DIR)
LFLAGS += -L$(LIBUIRQ_DIR)
LFLAGS += -L$(LIBEBUF_DIR)
LFLAGS += -L$(LIBEDMA_DIR)
LFLAGS += -L$(LIBESPI_DIR)
LFLAGS += -L$(LIBEFSPI_DIR)
LFLAGS += -L$(LIBPMEM_DIR)
LFLAGS += -L$(LIBEJTAG_DIR)

AR := $(DANCE_SDK_CROSS_COMPILE)ar
STRIP := $(DANCE_SDK_CROSS_COMPILE)strip


# application specific
CFLAGS += -DWITHIN_DANCE_CONTROLLER

# cannot override or not taken into account in rule evaluation
LIBDANCE_A := $(LIBDANCE_DIR)/libdance.a

DEP_LIBS += $(LIBDANCE_DIR)/libdance.a
DEP_HEADERS += $(LIBDANCE_DIR)/libdance.h
DEP_LIBS += $(LIBEPCI_DIR)/libepci.a
DEP_HEADERS += $(LIBEPCI_DIR)/libepci.h

#
# rules

.PHONY: all build buildlib buildall release clean cleanall updatesvn

# all usage
all:
	@echo "Usage: "
	@echo "    make build    : build controller"
	@echo "    make buildlib : build controller + libdance"
	@echo "    make buildall : equivalent to  cleanall + build"
	@echo "    make release  : check/update SVN + buildall"
	@echo "    make clean    : remove all locally generated files"
	@echo "    make cleanall : remove all both local files and dependenciessss"
	@echo "flags" $(CONTROLLER)

build: $(CONTROLLER)

buildlib: cleanlib build

buildall: cleanall build

release: RELEASE_CANDIDATE := *
release: clean libdancerelease updatesvn $(CONTROLLER)

clean :
	rm -f $(O_FILES)
	rm -f $(CONTROLLER)

cleanlib:
	rm -f $(LIBDANCE_A)

cleanall: clean
	$(call process_libdance, cleanall)

updatesvn:
	$(call svnupdate_to_myrecent,.)

libdancerelease:
	$(call process_libdance, release)


define process_libdance
	cd $(LIBDANCE_DIR) && $(MAKE) platform=$(DANCE_SDK_PLATFORM) $(1)
endef


$(CONTROLLER): $(CONTROLLER).o
	$(CC) -o $@ $(O_FILES) $(LFLAGS) -ldance -lepci -lpci -lefpak -lespi -lefspi -lz -ledma -luirq -lebuf -lpmem -lejtag -ltcc -lm -ldl -lrt -lpthread
	$(STRIP) $@
#$(H_FILES)
$(LIBDANCE_A): 
	$(call process_libdance, build)
#$(H_FILES)
$(CONTROLLER).o: $(CONTROLLER).c $(LIBDANCE_A) $(OTHER_O_FILES) 
	@$(call create_idfile,CONTROLLER,$(CONTROLLER))
	@$(call add_rcskeyword,Platform,$(DANCE_SDK_PLATFORM))
	@$(call add_rcskeyword,Tools,`$(CC) --version | head -n1`)
	@$(show_lastrcskwfile)
	$(CC) $(CFLAGS) -c -o $@ $<
	$(delete_idfile)

%.o: %.c $(H_FILES)
	$(CC) $(CFLAGS) -c -o $@ $<
