# called as: $(call new_rcskwfile,filename)
#
define new_rcskwfile
   $(eval _MYRCSKW_FILE = $(1))
   echo -n > $(_MYRCSKW_FILE)
endef

# called as: $(delete_rcskwfile)
#
delete_rcskwfile = rm $(_MYRCSKW_FILE)

# called as: $(call add_rcskeyword,keyword,value)
#
define add_rcskeyword
   echo "   \"\$$$(1): $(2) $$\\\n\"" >> $(_MYRCSKW_FILE)
endef

# called as: $(call add_rawkeyword,keyword,value)
#
define add_rawkeyword
   echo -n "\$$$(1): $(2) $$\\n" >> $(_MYRCSKW_FILE)
endef

# called as: $(show_lastrcskwfile)
#
define show_lastrcskwfile
   echo $(_MYRCSKW_FILE):
   cat $(_MYRCSKW_FILE)
endef


# used as: $(call svn_mostrecentrev,dir)
#
svn_mostrecentrev = $(shell svnversion $(1) | sed 's/[A-Z]//g;s/[0-9]*://g')

# used as: $(call svnmodifrev,dir)
#
svnmodifrev = $(shell svnversion $(1) | sed 's/[0-9:]*//')

# called as: $(call svnupdate_to_myrecent, dir)
#
svnupdate_to_myrecent = svn update $(1) -r $(call svn_mostrecentrev,$(1))

# used as: $(call svninfo_getkw,keyword,dir)
#
svninfo_getkw = $(shell svn info $(2) | sed -n 's/^$(1): //p')


# used as: $(call svninfo_getfullurl,dir)
#
svninfo_getfullurl = $(shell echo \
$(call svninfo_getkw,Repository Root,$(1))\
$(call svninfo_getkw,Relative URL,$(1)) | sed 's/ ^//')


# used as: $(call create_idfile,type,name,dir)
#
define create_idfile
   $(call new_rcskwfile,_idfile.h)
   $(call add_rcskeyword,Type,$(1))
   $(call add_rcskeyword,Name,$(2))
   $(call add_rcskeyword,URL,$(call svninfo_getfullurl,$(3)))
   $(call add_rcskeyword,Revrange,`svnversion $(3)`)
   if [ -n "$(RELEASE_CANDIDATE)" ] && [ -z "$(call svnmodifrev,$(3))" ]; then \
      $(call add_rcskeyword,Ready,*) ; \
   fi
   $(call add_rcskeyword,User,`whoami`)
   $(call add_rcskeyword,Date,`date '+%Y/%m/%d %H:%M:%S'`)
endef

# called as: $(delete_idfile)
#
delete_idfile = $(delete_rcskwfile)

#######################
# Bitfile tools
#######################
# used as: $(call create_bitfileinfo,type,name,dir)
#
define create_bitfileinfo
   $(call new_rcskwfile,headfile.bit)
   $(call add_rawkeyword,Type,$(1))
   $(call add_rawkeyword,Name,$(2))
   $(call add_rawkeyword,URL,$(call svninfo_getfullurl,$(3)))
   $(call add_rawkeyword,Revrange,`svnversion $(3)`)
   if [ -n "$(RELEASE_CANDIDATE)" ] && [ -z "$(call svnmodifrev,$(3))" ]; then \
      $(call add_rawkeyword,Ready,*) ; \
   fi
   $(call add_rawkeyword,User,`whoami`)
endef

# called as: $(call do_bitheader,bitfile_name)
#
define make_bitstreampkg
		$(eval HEAD_LENGTH = `stat -L -c %s headfile.bit`)
      printf '%04X %04X' 0xcafe $(HEAD_LENGTH) | xxd -r -ps > temphdr
		cat temphdr $(_MYRCSKW_FILE) > temphdr2
		cat temphdr2 $(1) > temphdr1
		mv temphdr1 $(1).pkg
		rm temphdr*
endef


# called as: $(call do_pack_bitstream,bitsteam_src,output_file)
#
define do_pack_bitstream
		$(call create_bitfileinfo,FPGA,Bitstream,./)
		$(do_bitheader)
endef
