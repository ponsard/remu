/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: remu   //  RASHPA emulator
 *
 * $URL: $
 * $Rev: $
 * $Date: $
 *------------------------------------------------------------------------- */


#ifndef __REMU_H_INCLUDED__
#define __REMU_H_INCLUDED__

#include <stdint.h>


// REMU data types

#define MAX_NAME_LENGTH 31

typedef struct {
   char           name[MAX_NAME_LENGTH + 1];
   size_t         nframes;
   uint32_t       hsz;    // horizontal & vertical image size in pixels
   uint32_t       vsz;
   uint32_t       depth;  // pixel depth in bytes
   void*          data;
} imgsequence_t;



typedef struct {

#define DS_UNUSED       0
#define DS_VARSZ  (1 << 0)
#define DS_FIXSZ  (1 << 1)
#define DS_IMAGE  (1 << 2)
#define DS_ANYTYPE  (DS_VARSZ | DS_FIXSZ | DS_IMAGE)
   int       type;   // type of data set

   char      name[MAX_NAME_LENGTH + 1];

   int       size;   // data set size in bytes
   int       bdepth; // pixel depth in bits
   void*     mskptr; // pointer to mask image data
} dataset_t;

#define DEF_TYPE   DS_UNUSED
#define DEF_IMSIZE 512
#define DEF_BDEPTH  16

#define DEFAULT_DATASET { \
   .type   = DEF_TYPE,    \
   .name   = "",          \
   .size   = DEF_IMSIZE * DEF_IMSIZE * (DEF_BDEPTH / 8), \
   .bdepth = DEF_BDEPTH,  \
   .mskptr = NULL,        \
}

#define MAX_N_DATASETS      5
#define MAX_N_IMGSEQUENCES 20

// permanent configuration data
typedef struct {
   uint32_t   hsz;    // horizontal image size in pixels
   uint32_t   vsz;    // vertical image size in pixels
   uint32_t   hoff;   // horizontal image offset in pixels
   uint32_t   voff;   // vertical image offset in pixels
   double     maxrate;
} remu_conf_t;

#define DEFAULT_REMUCONF {    \
   .hsz    = DEF_IMSIZE, \
   .vsz    = DEF_IMSIZE, \
   .hoff   = 0,          \
   .voff   = 0,          \
   .maxrate = 0,         \
}


typedef struct {
   uint32_t  in_use;
   uint32_t  dsetbits; // data set assignement bitmask
   size_t    size;     // data size in bytes
   mblock_t  mblkdata; // mblk structure to hold the image data
} mask_t;




typedef struct remu_globals_s {
   remu_conf_t*  conf;
   dataset_t*    dataset;  // set of datasets
   mask_t        masks[MAX_N_DATASETS];
   imgsequence_t imgseqs[MAX_N_IMGSEQUENCES];
} remu_globals_t ;


extern remu_globals_t* g_remu;

#endif  //  __REMU_H_INCLUDED__
